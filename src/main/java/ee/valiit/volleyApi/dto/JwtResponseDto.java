package ee.valiit.volleyApi.dto;

public class JwtResponseDto {

    private final int id;
    private final String username;
    private final String token;
    private final int isAdmin;

    public JwtResponseDto(int id, String username, String token, int isAdmin) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return this.token;
    }

    public int getIsAdmin() {
        return isAdmin;
    }
}
