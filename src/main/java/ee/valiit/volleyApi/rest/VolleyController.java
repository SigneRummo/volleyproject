package ee.valiit.volleyApi.rest;

import ee.valiit.volleyApi.data.BookingData;
import ee.valiit.volleyApi.data.TrainingSessionData;
import ee.valiit.volleyApi.data.UserData;
import ee.valiit.volleyApi.dto.*;
import ee.valiit.volleyApi.service.VolleyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")

public class VolleyController {
    @Autowired
    private VolleyService volleyService;

    // Küsime kõigi aegade tabeli
    @GetMapping("/sessions")
    public List<TrainingSessionData> fetchTrainingSessions() {
        return volleyService.fetchTrainingSessions();
    }

    // Küsime kõik kasutajad
    @GetMapping("/users")
    public List<UserData> fetchUsers() {
        return volleyService.fetchUsers();
    }

    // küsime ühe sessiooni broneeringuid
    @GetMapping("/bookings")
    public List<UserData> fetchBookings(@RequestParam("id") int id) {
        return volleyService.fetchBookings(id);
    }


    @GetMapping("/pendingbookings")
    public List<UserData> fetchPendingBookings(@RequestParam("id") int id) {
        return volleyService.fetchPendingBookings(id);
    }


    /*// küsime ühe kasutaja broneeringuid
    @GetMapping("/oneuserbookings")
    public List<BookingData> fetchOneUserBookings(@RequestParam("id") int userId) {
        return volleyService.fetchOneUserBookings(userId);
    }*/

    // Admin lisab sessiooni aja
    @PostMapping("/session")
    public void addSession(
            @RequestBody TrainingSessionData x) {
        volleyService.addSession(x);
    }

    // Kasutaja liitub
    @PostMapping("/user")
    public void addUser(
            @RequestBody UserData y) {
        volleyService.addUser(y);
    }

    // Kasutaja muudab andmeid
    @PutMapping("/user")
    public void updateUser(@RequestBody UserData q) {
        volleyService.updateUser(q);
    }

    /*@PutMapping("/password")
    public void updatePassword (@RequestBody UserData q, @RequestParam("username") String username) {
        volleyService.updatePassword(q, username);}*/

    // Krediidiseisu muutmine
    @PutMapping("/usercredit")
    public void updateUserCredit(@RequestParam("userId") int userId, @RequestParam("newBalance") int newBalance) {
        volleyService.updateUserCredit(userId, newBalance);
    }

    // Admin kustutab sessiooni
    @DeleteMapping("/session")
    public void deleteSession(@RequestParam("id") int id) {
        volleyService.deleteSession(id);
    }

    // Kasutaja kustutamine
    @DeleteMapping("/user")
    public void deleteUser(@RequestParam("id") int id) {
        volleyService.deleteUser(id);
    }

    // Otsi kasutajat
    @GetMapping("/user")
    public UserData getUser(@RequestParam("id") int id) {
        return volleyService.fetchUser(id);
    }

    // Broneeringu lisamine

    @PostMapping("/booking")
    public ResultInfoDto addBooking(@RequestParam("sessionId") int sessionId) {
        return volleyService.addBooking(sessionId);
    }

    /*@PostMapping("/booking")
    public ResultInfoDto addPendingBooking(@RequestParam("sessionId") int sessionId) {
        return volleyService.addPendingBooking(sessionId);
    }*/
    // Kasutaja kõigi broneeringute otsimine


    // Broneeringu kustutamine
    @DeleteMapping("/booking")
    public void deleteBooking(@RequestParam("sessionId") int sessionId) {volleyService.deleteBooking(sessionId);}

    /*@GetMapping("/isSessionDateExpired")
    public String isSessionDateExpired (String getCurrentDate, String sessionDate) {
        String response = "false";
        int getCurrentDateInt = Integer.parseInt(getCurrentDate);
        int sessionDateInt = Integer.parseInt(sessionDate);
        if (getCurrentDateInt < sessionDateInt) {
            response = "true";
        } return response;
    }*/


    // Autentimisega seotud
    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return volleyService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return volleyService.authenticate(request);
    }
}

