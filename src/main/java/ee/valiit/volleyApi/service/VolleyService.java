package ee.valiit.volleyApi.service;

import java.util.List;

import ee.valiit.volleyApi.data.BookingData;
import ee.valiit.volleyApi.data.TrainingSessionData;
import ee.valiit.volleyApi.data.UserData;
import ee.valiit.volleyApi.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

@org.springframework.stereotype.Service
public class VolleyService {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<TrainingSessionData> fetchTrainingSessions() {
        List<TrainingSessionData> sessions = jdbcTemplate.query(
                "SELECT * FROM training_session WHERE session_deleted = 0 ORDER BY date ASC", (row, rowNum) -> {
                    return new TrainingSessionData(
                            row.getInt("id"),
                            row.getString("date"));

                });
        for (TrainingSessionData session : sessions) {
            List<UserData> bookedUsers = fetchBookings(session.getId());
            session.setBookings(bookedUsers);

            List<UserData> pendingUsers = fetchPendingBookings(session.getId());
            session.setPendingBookings(pendingUsers);
        }
        return sessions;
    }

    //where isdeleted != 1
    public List<UserData> fetchUsers() {
        List<UserData> users = jdbcTemplate.query(
                "SELECT * FROM user ORDER by first_name ASC", (row, rowNum) -> {
                    return new UserData(
                            row.getInt("id"),
                            row.getString("username"),
                            null,
                            row.getString("first_name"),
                            row.getString("last_name"),
                            row.getString("email"),
                            row.getString("phone_nr"),
                            row.getInt("is_admin"),
                            row.getInt("credit_balance"));

                });
        return users;
    }

    public List<UserData> fetchBookings(int id) {
        List<UserData> bookings = jdbcTemplate.query(
                "SELECT * FROM projekt.user " +
                        "INNER JOIN projekt.user_booking ON user_booking.user_id = user.id " +
                        "INNER JOIN training_session ON training_session.id = user_booking.training_session_id " +
                        "WHERE training_session_id = ? " +
                        "AND training_session.session_deleted = 0 " +
                        "AND user_booking.`status` = 'confirmed'  " +
                        "ORDER by user_booking.booking_time ASC", new Object[]{id}, (row, rowNum) -> {
                    return new UserData(
                            row.getInt("id"),
                            row.getString("first_name"),
                            row.getString("last_name"),
                            row.getString("email"),
                            row.getString("phone_nr"));
                });


      //  List<UserData> firstTwelve = bookings.subList(0, Math.min(4, bookings.size()));
      //    for (int i = 0; i < 12; i++) {
       //     bookings.get(i).isAccepted();


        return bookings;
    }


        public List<UserData> fetchPendingBookings(int id) {
        List<UserData> pendingBookings = jdbcTemplate.query(
                "SELECT * FROM projekt.user " +
                        "INNER JOIN projekt.user_booking ON user_booking.user_id = user.id " +
                        "INNER JOIN training_session ON training_session.id = user_booking.training_session_id " +
                        "WHERE training_session_id = ? " +
                        "AND training_session.session_deleted = 0 " +
                        "AND user_booking.`status` = 'pending'  " +
                        "ORDER by user_booking.booking_time ASC", new Object[]{id}, (row, rowNum) -> {
                    return new UserData(
                            row.getInt("id"),
                            row.getString("first_name"),
                            row.getString("last_name"),
                            row.getString("email"),
                            row.getString("phone_nr"));
                });



        return pendingBookings;
    }

    public void addSession(TrainingSessionData session) {
        jdbcTemplate.update("INSERT INTO training_session (date) VALUE (?)",
                session.getDate());
    }

    public void addUser(UserData user) {
        jdbcTemplate.update("INSERT INTO user (username, password, first_name, last_name, email, phone_nr) VALUES (?, ?, ?, ?, ?, ?)",
                user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(), user.geteMail(), user.getTelNr());
    }

    public void updateUser(UserData user) {
        if (user.getPassword() != null && user.getPassword().length() > 0) {
            String password = passwordEncoder.encode(user.getPassword());
            jdbcTemplate.update("UPDATE user SET first_name = ?, last_name = ?, email = ?, phone_nr = ?, password = ? WHERE id = ?",
                    user.getFirstName(), user.getLastName(), user.geteMail(), user.getTelNr(), password, getLoggedInUserId());
        } else {
            jdbcTemplate.update("UPDATE user SET first_name = ?, last_name = ?, email = ?, phone_nr = ? WHERE id = ?",
                    user.getFirstName(), user.getLastName(), user.geteMail(), user.getTelNr(), getLoggedInUserId());

        }
    }
    /*public void updatePassword(UserData user) {
        jdbcTemplate.update("UPDATE user SET password = ?, first_name = ?, last_name = ?, email = ?, phone_nr = ? WHERE username = ?",
                user.getPassword(), user.getFirstName(), user.getLastName(), user.geteMail(), user.getTelNr(), user.getUsername());
    }*/

    public void updateUserCredit (int userId, int newBalance) {
        jdbcTemplate.update("UPDATE user SET credit_balance = credit_balance + ? WHERE id = ?", newBalance, userId);
    }

    public void deleteSession(int id) {
        jdbcTemplate.update("UPDATE training_session SET session_deleted = 1 WHERE id = ?", id);
    }

    public void deleteUser(int id) {
        jdbcTemplate.update("DELETE FROM user WHERE id = ?", id);
    }

    public void deleteBooking(int sessionId) {
        jdbcTemplate.update("UPDATE user_booking SET `status` = 'deleted' WHERE user_id = ? AND training_session_id = ?", getLoggedInUserId(), sessionId);
        jdbcTemplate.update( "UPDATE user SET credit_balance = credit_balance + 5 WHERE id = ?", getLoggedInUserId());
    }

    public ResultInfoDto addBooking(int sessionId) {
        if (!bookingExist(sessionId) && fetchBookings(sessionId).size() < 12) {
            jdbcTemplate.update("INSERT INTO user_booking (user_id, training_session_id, `status`) values (?, ?, 'confirmed')", getLoggedInUserId(), sessionId);
            jdbcTemplate.update( "UPDATE user SET credit_balance = credit_balance - 5 WHERE id = ?", getLoggedInUserId());
            return new ResultInfoDto("OK", "Lisamine õnnestus");
        } else if (!bookingExist(sessionId) && fetchBookings(sessionId).size() >= 12) {
            jdbcTemplate.update("INSERT INTO user_booking (user_id, training_session_id, `status`) values (?, ?, 'pending')", getLoggedInUserId(), sessionId);
            return new ResultInfoDto("OK2", "Lisatud ootenimekirja");
        } else {
            return new ResultInfoDto("FAIL", "Broneering oli juba olemas");
        }
    }

    /*public boolean isThereFreeSpace (int sessionId) {
        if (fetchBookings(sessionId).size() < 4) {
            return true;
        }else return false;
    }*/


    public int doesBookingExist(int userId, int sessionId) {
        return jdbcTemplate.queryForObject("SELECT COUNT(booking_time) FROM user_booking " +
                "WHERE user_id = ? AND training_session_id = ? AND `status` = 'confirmed'", new Object[]{userId, sessionId}, Integer.class);
    }

   public boolean bookingExist(int sessionId) {
        int userId = getLoggedInUserId();
        if (doesBookingExist(userId, sessionId) == 0) {
            return false;
        }
        return true;
    }
    public boolean isUserAdmin(int userIsAdmin) {
        if (volleyService.getLoggedInUserIsAdmin() == 1) {
            return true;
        }
        return false;
    }


    public int getLoggedInUserId() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        UserData user = getUserByUsername(username);

        int userId = user.getId();
        return userId;
    }

    public int getLoggedInUserIsAdmin() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        UserData user = getUserByUsername(username);

        int userIsAdmin = user.getIsAdmin();
        return userIsAdmin;
    }

    public UserData fetchUser(int id) {
        List<UserData> users = jdbcTemplate.query(
                "SELECT * FROM user WHERE id = ?",
                new Object[]{id},
                (row, rowNum) -> {
                    return new UserData(
                            row.getInt("id"),
                            row.getString("username"),
                            null,
                            row.getString("first_name"),
                            row.getString("last_name"),
                            row.getString("email"),
                            row.getString("phone_nr"),
                            row.getInt("is_admin"),
                            row.getInt("credit_balance"));
                });
        return users.size() > 0 ? users.get(0) : null;
    }

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }


    public UserData getUserByUsername(String username) {
        List<UserData> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new UserData(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getInt("is_admin"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }


    @Autowired
    private VolleyService volleyService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        UserData user = new UserData(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()),
                userRegistration.getFirstName(), userRegistration.getLastName(), userRegistration.geteMail(),
                userRegistration.getTelNr());
        if (!volleyService.userExists(userRegistration.getUsername())) {
            volleyService.addUser(user);
        } else {
            responseDto.getErrors().add("Sellise aadressiga kasutaja on juba olemas.");
        }
        return responseDto;
    }

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final UserData userDetails = volleyService.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token, userDetails.isAdmin());
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}


