package ee.valiit.volleyApi.data;

import java.util.List;

public class TrainingSessionData {
    private int id;
    private String date;
    private List<UserData> bookings;
    private List<UserData> pendingBookings;

    public TrainingSessionData(int id, String date) {
        this.id = id;
        this.date = date;
    }

    public TrainingSessionData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<UserData> getBookings() {
        return bookings;
    }
    public List<UserData> getPendingBookings() {
        return pendingBookings;
    }

    public void setBookings(List<UserData> bookings) {
        this.bookings = bookings;
    }
    public void setPendingBookings(List<UserData> pendingBookings) {
        this.pendingBookings = pendingBookings;
    }
}
