package ee.valiit.volleyApi.data;

public class BookingData {
    private int userId;
    private int trainingSessionId;
    private String timeStamp;
    private String status;

    public BookingData() {
    }

    public BookingData(int userId, int trainingSessionId, String timeStamp, String status) {
        this.userId = userId;
        this.trainingSessionId = trainingSessionId;
        this.timeStamp = timeStamp;
        this.status = status;
    }

    public BookingData(int userId, int trainingSessionId, String timeStamp) {
        this.userId = userId;
        this.trainingSessionId = trainingSessionId;
        this.timeStamp = timeStamp;
    }

    public String getStatus() { return status;   }

    public void setStatus(String status) { this.status = status; }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTrainingSessionId() {
        return trainingSessionId;
    }

    public void setTrainingSessionId(int trainingSessionId) {
        this.trainingSessionId = trainingSessionId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
