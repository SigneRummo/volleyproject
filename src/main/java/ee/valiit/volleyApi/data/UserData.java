package ee.valiit.volleyApi.data;

import java.util.ArrayList;
import java.util.List;

public class UserData {
    private int id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String eMail;
    private String telNr;
    private int creditBalance;
    private int isAdmin;
// private boolean accepted;


    public int getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(int creditBalance) {
        this.creditBalance = creditBalance;
    }

    public UserData() {}

    public UserData(int id, String username, String password, String firstName, String lastName, String eMail, String telNr, int isAdmin) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
        this.isAdmin = isAdmin;
    }

    public UserData(int id, String username, String password, String firstName, String lastName, String eMail, String telNr, int isAdmin, int creditBalance) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
        this.creditBalance = creditBalance;
        this.isAdmin = isAdmin;
    }

    public int isAdmin() {
        return isAdmin;
    }

    public void setAdmin(int admin) {
        isAdmin = admin;
    }
/* public UserData(int id, String username, String password, String firstName, String lastName, String eMail, String telNr, boolean accepted) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
        this.accepted = accepted;
    }*/

    public UserData(int id, String username, String password, String firstName, String lastName, String eMail, String telNr) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
    }

    public UserData(int creditBalance) {
        this.creditBalance = creditBalance;
    }
/*public UserData(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }*/

    public UserData(int id, String firstName, String lastName, String eMail, String telNr) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
    }

    public UserData(int id, String username, String password, int isAdmin) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public UserData(int id, String first_name, String last_name, String username, String password, int credit_balance, int is_admin) {
        this.username = username;
        this.password = password;
    }

    public UserData(String username, String password, String firstName, String lastName, String eMail, String telNr) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.telNr = telNr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getTelNr() {
        return telNr;
    }

    public void setTelNr(String telNr) {
        this.telNr = telNr;
    }

    public int getIsAdmin() {
        return isAdmin;
    }
}



