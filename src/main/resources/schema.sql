DROP TABLE IF EXISTS `user_booking`;
DROP TABLE IF EXISTS `training_session`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
      `username` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `password` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `phone_nr` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
      `credit_balance` int(50) NOT NULL DEFAULT 0,
      `is_admin` bit(1) NOT NULL DEFAULT b'0',
      PRIMARY KEY (`id`)

);

CREATE TABLE IF NOT EXISTS `training_session` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
      `date` date NOT NULL,
      `session_deleted` bit(1) NOT NULL DEFAULT b'0',
      PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `user_booking` (
    `user_id` int(11) NOT NULL,
      `training_session_id` int(11) NOT NULL,
      `booking_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
      `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
      KEY `FK_user_booking_user` (`user_id`),
      KEY `FK_user_booking_training_session` (`training_session_id`),
      CONSTRAINT `FK_user_booking_training_session` FOREIGN KEY (`training_session_id`) REFERENCES `training_session` (`id`),
      CONSTRAINT `FK_user_booking_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);