INSERT INTO `user` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `phone_nr`, `credit_balance`, `is_admin`) VALUES
	(1, 'admin@admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei', 'Aadik', 'Admin', 'admin@admin', '444555', 28, b'1'),
	(5, 'edgar@gmail.com', '$2a$10$zwNeXxSivPWeot4gPy8b8.M4Fb09/VUgeKU38qIujJsJ/iDyRhMoi', 'Edgar', 'Savisaar', 'edgar@gmail.com', '54492456', 15, b'0'),
	(7, 'merle@gmail.com', '$2a$10$60V6JSym0zn16BLmOi3fVuamvwxfxdKerTY./woHTVV6J.wN7WVpa', 'Merle', 'Videvik', 'merle@gmail.com', '54492456', 1, b'0'),
	(8, 'maasik@gmail.com', '$2a$10$Q9NWLd5wVr8T9MXlPKujy.xcvcmY.SG8XCUmHTwQxNt44njrq.InK', 'Kaja', 'Maasik', 'maasik@gmail.com', '58994335', 0, b'0'),
	(10, 'kati@gmail.com', '$2a$10$9yqITugzKdrqMKbD964xouiZH2K6UdWOMOo2GVxB2p4pWrXIXFJDK', 'Uudo', 'Uusik', 'kati@gmail.com', '51992456', 0, b'0'),
	(12, 'jüri@juri.com', '$2a$10$3hGGfnwmSHXNnve5mZTHAuou2bANdRXl6mM/dv0kWcpgUj8Qd6clS', 'Jüri', 'Ratas', 'jüri@juri.com', '9876543', 20, b'0'),
	(14, 'kaja@kallas.ee', '$2a$10$dXlITZRxVJVGBECZx57uJ.Pb7UzhTINe5/ol3QA17Tyr2B7NP8OGG', 'Kaja', 'Kallas', 'kaja@kallas.ee', '6789054', 0, b'0'),
	(15, 'mart.rummo@gmail.com', '$2a$10$9SkUFol/DgYrJOHtLaV3oOBxJGiQeUYzCaEsYAT0TClknwGq6zu2K', 'Mart', 'Rummo', 'mart.rummo@gmail.com', '56649194', 40, b'1'),
	(17, 'karl@gmail.com', '$2a$10$xjVYu/Fo9QY4afdTUdoPs.VULiZAZ9AeKG3POrPNJMTRSsrb7LGRC', 'Karl', 'Kaasik', 'karl@gmail.com', '58994335', 5, b'0'),
	(20, 'kadri@kapp.ee', '$2a$10$37Y4ou7uNjwSMwoipq788.I3H3KyVqqMPbX2odRXO6sn7Z0RtyIZe', 'Kadri', 'Kapp', 'kadri@kapp.ee', '55667788', 0, b'0'),
	(25, 'mia.rummo@gmail.com', '$2a$10$KdKXLF9Pu12OiSDQPSKEGeP9oB2Z/v2PpFyymo.tuqzJl/r.lVdEe', 'Mia ', 'Rummo', 'mia.rummo@gmail.com', '55559843', 0, b'0'),
	(41, 'signe.rummo@gmail.com', '$2a$10$fbjw1EfUHtl0U6KVvSuQd.m38koP6ZiIivBQIYqps2eomD/h8./Mi', 'Signe', 'Rummo', 'signe.rummo@gmail.com', '44668899', 0, b'0'),
	(48, 'mari@mari', '$2a$10$xjVYu/Fo9QY4afdTUdoPs.VULiZAZ9AeKG3POrPNJMTRSsrb7LGRC', 'Mari', 'Maasikas', 'mari@mari', '66775588', 0, b'0');

INSERT INTO `training_session` (`id`, `date`, `session_deleted`) VALUES
	(1, '2019-12-04', b'0'),
	(7, '2019-11-09', b'1'),
	(12, '2019-10-13', b'1'),
	(13, '2019-09-23', b'0'),
	(14, '2019-09-25', b'0'),
	(15, '2019-12-09', b'0'),
	(16, '2019-11-09', b'1'),
	(17, '2019-10-09', b'0'),
	(18, '2019-09-21', b'1'),
	(19, '2019-09-24', b'1'),
	(20, '2019-12-19', b'1'),
	(21, '2019-10-04', b'0'),
	(22, '2019-12-19', b'0'),
	(23, '2019-09-26', b'0');

INSERT INTO `user_booking` (`user_id`, `training_session_id`, `booking_time`, `status`) VALUES
	(1, 7, '2019-09-12 14:53:43', 'confirmed'),
	(1, 13, '2019-09-17 18:22:47', 'confirmed'),
	(7, 1, '2019-09-17 18:39:14', 'confirmed'),
	(10, 1, '2019-09-13 19:33:43', 'confirmed'),
	(5, 14, '2019-09-22 13:56:17', 'confirmed'),
	(20, 13, '2019-09-17 15:35:42', 'confirmed'),
	(17, 13, '2019-09-17 15:36:09', 'confirmed'),
	(15, 12, '2019-09-22 13:44:13', 'confirmed'),
	(15, 1, '2019-09-22 13:44:03', 'confirmed'),
	(12, 15, '2019-09-18 14:43:11', 'confirmed'),
	(15, 21, '2019-09-19 16:05:14', 'confirmed'),
	(15, 23, '2019-09-22 13:44:38', 'pending'),
	(15, 15, '2019-09-20 16:15:21', 'confirmed'),
	(15, 13, '2019-09-21 15:16:00', 'confirmed'),
	(15, 17, '2019-09-21 15:18:30', 'confirmed'),
	(15, 22, '2019-09-21 18:42:20', 'confirmed'),
	(1, 23, '2019-09-22 13:54:35', 'pending'),
	(5, 23, '2019-09-19 16:48:47', 'confirmed'),
	(12, 23, '2019-09-22 13:44:24', 'confirmed'),
	(20, 23, '2019-09-19 16:48:47', 'confirmed'),
	(14, 23, '2019-09-19 16:48:47', 'confirmed'),
	(17, 23, '2019-09-19 16:48:47', 'confirmed'),
	(10, 23, '2019-09-19 16:48:47', 'confirmed'),
	(8, 23, '2019-09-19 16:48:47', 'confirmed'),
	(7, 23, '2019-09-19 16:48:47', 'confirmed'),
	(25, 23, '2019-09-19 16:48:47', 'confirmed'),
	(41, 23, '2019-09-19 16:48:47', 'confirmed'),
	(48, 23, '2019-09-19 16:48:47', 'confirmed'),
	(12, 14, '2019-09-22 13:56:20', 'confirmed'),
	(20, 14, '2019-09-22 13:56:23', 'confirmed'),
	(14, 14, '2019-09-22 13:56:28', 'confirmed'),
	(17, 14, '2019-09-22 13:56:31', 'confirmed'),
	(10, 14, '2019-09-22 13:56:37', 'confirmed'),
	(8, 14, '2019-09-22 13:56:40', 'confirmed'),
	(48, 14, '2019-09-22 13:56:43', 'confirmed'),
	(15, 14, '2019-09-22 13:58:28', 'pending'),
	(7, 14, '2019-09-22 13:56:53', 'confirmed'),
	(25, 14, '2019-09-22 13:56:57', 'confirmed'),
	(41, 14, '2019-09-22 13:57:01', 'confirmed'),
	(1, 14, '2019-09-22 13:45:55', 'confirmed');